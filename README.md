[![Swift](https://img.shields.io/badge/Swift-5.7_5.8-orange?style=flat-square)](https://img.shields.io/badge/Swift-5.7_5.8-Orange?style=flat-square)
[![iOS](https://img.shields.io/badge/iOS-14+-blue?style=flat-square)](https://img.shields.io/badge/iOS-14-blue?style=flat-square)
[![LICENSE](https://img.shields.io/badge/LICENSE-MIT-black?style=flat-square)](https://img.shields.io/badge/iOS-14-blue?style=flat-square)

# M800CoreSDK


## iOS Quick Start Guide

To quickly integrate the `M800CoreSDK` framework into your iOS application, follow these steps:

1. Open your Xcode project and navigate to the project directory.
2. Open the `Podfile` file and add the following line:
```
    pod 'M800CoreSDK'
```
